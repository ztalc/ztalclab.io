import { TeacherDivs, removeClass } from './zen-timeline/teacher_divs.js';

// Prevent TypeError is null error from js loading before page.
window.onload=function() {
  // Assign ids to teacher divs.
  const teacherDivs = new TeacherDivs(document.getElementsByClassName('teacher'));
  teacherDivs.prepDivs();
}
