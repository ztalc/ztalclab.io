export const dynastyData = [
  { content: "Northern & Southern",
    start: "0420",
    end: "0589",
    type: "range",
    className: "northern-southern"
  },
  { content: "Sui",
    start: "0581",
    end: "0618",
    type: "range",
    className: "sui-dynasty"
  },
  { content: "Tang",
    start: "0618",
    end: "0907",
    type: "range",
    className: "tang-dynasty"
  },
  // { content: "Zhou interregnum",
  //   start: "0690",
  //   end: "0705",
  //   type: "range",
  //   className: "zhou"
  // },
  { content: "Five Dynasties &<br> Ten Kingdoms",
    start: "0907",
    end: "0979",
    type: "range",
    className: "ten-kingdoms"
  },
  { content: "Northern Song",
    start: "0960",
    end: "1127",
    type: "range",
    className: "northern-song"
  },
  { content: "Southern Song",
    start: "1127",
    end: "1279",
    type: "range",
    className: "southern-song"
  },
  { content: "Khitan/Liao",
    start: "916",
    end: "1125",
    type: "range",
    className: "liao"
  },
  { content: "Jin",
    start: "1115",
    end: "1234",
    type: "range",
    className: "jin"
  },
  { content: "Qara Khitai",
    start: "1124",
    end: "1218",
    type: "range",
    className: "qara-khitai"
  },
  { content: "Mongol Empire",
    start: "1206",
    end: "1368",
    type: "range",
    className: "mongol"
  },
  { content: "Yuan",
    start: "1271",
    end: "1368",
    type: "range",
    className: "yuan"
  },
];
