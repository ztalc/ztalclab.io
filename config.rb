# Activate and configure extensions
# https://middlemanapp.com/advanced/configuration/#configuring-extensions

require 'slim'

activate :livereload
activate :autoprefixer do |prefix|
  prefix.browsers = "last 2 versions"
end

# Webpack
activate :external_pipeline,
  name: :webpack,
  command: build? ?
   "./node_modules/webpack/bin/webpack.js --bail --mode production --env production" :
   "./node_modules/webpack/bin/webpack.js --watch --progress --color",
  source: ".tmp/dist",
  latency: 1

# Dev environment
configure :development do
 config[:css_dir] = ".tmp/dist"
 config[:js_dir] = ".tmp/dist"
end

# Layouts
# https://middlemanapp.com/basics/layouts/

# Per-page layout changes
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

# With alternative layout
# page '/path/to/file.html', layout: 'other_layout'

# Proxy pages
# https://middlemanapp.com/advanced/dynamic-pages/

# proxy(
#   '/this-page-has-no-template.html',
#   '/template-file.html',
#   locals: {
#     which_fake_page: 'Rendering a fake page with a local variable'
#   },
# )

# Helpers
# Methods defined in the helpers block are available in templates
# https://middlemanapp.com/basics/helper-methods/

# helpers do
#   def some_helper
#     'Helping'
#   end
# end

# Build-specific configuration
# https://middlemanapp.com/advanced/configuration/#environment-specific-settings

configure :build do
  #   Webpack should handle this.
  #   activate :minify_css
  #   activate :minify_javascript
  activate :asset_hash
  activate :gzip
  # requires gem "middleman-minify-html"
  activate :minify_html
end
