# TODO
Create multiple webpack entry points to break up js files.
analytics
patreon
mobile tests
Figure out a way to convert teachers.js to teachers.json
Events
Fix: disable horizontal scroll, timeline width
Get order of teachers visually correct.
Hide all other teacher except selected? Too difficult.
Be able to select multiple teachers. Why?
Show friendships/encounters?
Immediate descendants vs all descendants
Add other schools
Decide Japanese/Korean/Vietnamese colours
Sidebar in about page shows contents.

Clicking on background should clear lineages. Attempted and failed!

Provide svg version of chart.
Increase timeline size with text zoom so that users can find the balance between readability and screen size

Browser compatibility.

Buttons to highlight various schools. Not really necessary. Already highlighted.
search bar - Find cannot highlight parent divs easily via css. JS solution?

Ally
- use graphic to show ancestors and descendants, not just colour.
- sitemap
- tab through teachers?? Numbering teachers would interfere with tabbing through nav etc.

Webpack creates style.min.js when it shouldn't. Fix is in webpack.config.js.

# Done
Change cursor on hover. No other icon is more appropriate.
Make text pages more readable.
Get rid of links cos they get in the way of fingers. Create pop up with link?
Remove vis-timeline click event.
Clicking on background deselects selected teacher, but not ancestors/descendants.
convert to json
Create precompile_ancestors.
Style teachers
leaderlines
add schools
Selected teacher fade is too abrupt.
Shaded teacher-mark. Add gradient to uncertain dates.
Fade schools
Right and left fade are broken.
Fix school gradient transparency.Fade schools
Make legend hideable
Get rid of autoprefixer (deprecated) if possible. This is a minor issue that can only be fixed by middleman-autoprefixer gem.

# Webpack 5 pipeline
Sprockets no longer works with Middleman 4.
Follow instructions:
https://dev.to/pmallol/add-webpack-to-a-middleman-app-3npb

Remove -d flag from config.rb under "activate :external_pipeline"

## Loading pregenerated html
To create a page with the timeline html already generated:
- save js generated page as complete web page
- extract the timeline html
- convert to slim (fix errors)
- put in partial
- remove redundant html? (redundant parts: .vis-background, anything with "hidden"). Ultimately, the only thing that needs to be changed is .teacher-group section.
- add any a11y stuff
- serve on separate page.

## Install npm modules
Run npm install foo, for whatever modules you require.
When you get "foo requires a peer of bar..." warning, add bar to package.json with the exact version number in the warning. Then delete node_modules dir and run npm install again. This is the only way to get rid of cli warning messages and browser error messages when trying to load modules.

If css files do not update, and you get a node-sass error in the console, run:
```
npm rebuild node-sass
```

To compile/minify:
```
npx webpack
```
Output is in .tmp/dist

# SVG chart
# Updating teachers and ancestors_index
After you update teachers, you must manually convert teachers.js to teachers.json, then run ~/bin/precompile_ancestors.rb and copy file to data dir.

## About teachers.js file
Order of teachers must be in order of seniority, but ids do not have to match array order.
Id is a vis-timeline convention. Not used by me.
Item_id is the id assigned to div#id and must match the relationship between teachers.
From the 6th generation, first digit of Item_id denotes generation, other digits have no meaning.
Dharma ancestors must have lower id than descendants, ie ids within generations can be random, but between generations must be sequential.
This enables new teachers to be added without having to rewrite ids (too strictly).
Descendants is a convenience.

Update source/javascripts/data/teachers.js manually, then convert to json via
https://www.convertsimple.com/convert-javascript-to-json/

# Performance
JSON data seems slower than js array? Test again when you have more teachers.
At commit 59672a9, measured by Firefox (empty cache):
Cached responses: 0
Total requests: 8
Size: 2,024.92 KB
Transferred Size: 2,026.22 KB
Time: 1.07 seconds
Non blocking time: 1.07 seconds

At commit 360806e, measured by Firefox (empty cache):
JS generated page
Cached responses: 0
Total requests: 14
Size: 2,354.49 KB
Transferred Size: 2,352.51 KB
Time: 1.75 seconds
Non blocking time: 1.54 seconds

Preloaded html page with redundant JS:
Cached responses: 0
Total requests: 10
Size: 2,421.48 KB
Transferred Size: 2,422.98 KB
Time: 1.55 seconds
Non blocking time: 1.53 seconds
