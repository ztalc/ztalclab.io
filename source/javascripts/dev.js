import { DataSet } from "vis-data/peer";
import { Timeline } from "vis-timeline/peer";
// To use json, use "export default" or it barfs. Not true.
// export default teacherData from './data/teachers.json';
import { teacherData } from './data/teachers.js';
import { dynastyData } from './data/dynasties.js';
// import { rulerData } from './data/rulers.js';
// import { eventData } from './data/events.js';
import { Items } from './zen-timeline/items.js';
import { TeacherDivs, removeClass } from './zen-timeline/teacher_divs.js';

// Prevent TypeError is null error from js loading before page.
window.onload=function() {
  // Order is important for performance and to prevent race conditions!
  const container = document.getElementById("visualization");
  const timeline = new Timeline(container);

  // Groups must be assigned first to prevent race condition.
  const groups = new DataSet([
    {id: 0, className: "dynasty-group", content: ''},
    {id: 1, className: "teacher-group", content: ''},
    // {id: 1, className: "event-group", content: 'Events'},
    // {id: 2, className: "ruler-group", content: 'Rulers'},
  ]);
  timeline.setGroups(groups);

  // function orderById (a, b) {
  //   return a.order_id - b.order_id;
  // }

  // Determine appropriate width of timeline.
  // var timelineWidth = () => {
  //   let width = "100%";
  //   if (window.innerWidth <= 1) {
  //     // A width wide enough to show names (dependant on timeline size).
  //     width = "2400px";
  //   }
  //   return width;
  // }

  // Configuration for the Timeline
  const options = {
    // format: {minorLabels: {year: "YY"}},
    // minHeight: "300px",
    // percent does not seem to work.
    // maxHeight: window.innerHeight,
    // This will change as the timeline gets bigger.
    // Otherwise names become unreadable.
    // width: timelineWidth(),
    width: '2300px',
    horizontalScroll: false,
    verticalScroll: false,
    selectable: false,
    showMajorLabels: false,
    dataAttributes: ['descendant_ids', 'item_id'],
    orientation: {axis: "both", item: "top"},
    // timeAxis: {scale: 'hour', step: 50},
    // order: orderById,
    zoomable: false,
    // Performance improvement
    start: 450,
    end: 1300,
  };
  timeline.setOptions(options);

  const items = new DataSet();
  // Add metacontent
  for (var i = 0, len = teacherData.length; i < len; i++) {
    if (typeof(teacherData[i].metacontent) != 'undefined') {
      teacherData[i].content = "<span>" + teacherData[i].content + '</span><span>' + teacherData[i].metacontent + "</span>";
    }
  }
  // items.add(new Items(eventData, "events").items);
  items.add(new Items(dynastyData, "dynasties").items);
  // items.add(new Items(rulerData, "rulers").items);
  items.add(new Items(teacherData, "teachers").items);

  // Create a Timeline
  timeline.setItems(items);

  // Assign ids to teacher divs.
  const teacherDivs = new TeacherDivs(document.getElementsByClassName('teacher'));
  teacherDivs.prepDivs();

  // var toggleTeachers = () => {
  //   console.log('toggle teachers!');
  //   var teachersToHide = document.querySelectorAll('.teacher:not(.ancestor):not(.selected-teacher):not(.descendant)');
  //   for(let teacher of teachersToHide){
  //     teacher.style.display = "none";
  //   }
  //   // var remainingTeachers = document.
  //   // const teacherDivs = new TeacherDivs(document.getElementsByClassName('teacher'));
  //   // teacherDivs.prepDivs();
  // }

  //Show/hide unselected teachers
  // document.getElementById('toggle-teachers').addEventListener("click", toggleTeachers, false);

  // Does not work because timeline object captures clicks on teachers.
  // This prevents the same teacher from being reselected without clicking twice.
  // Preventing drag events from deselecting teachers is a pain.
  // Adding eventlistener to #visualization or other element does not work. Had to use timeline object.
  // var click = true;
  // var clickTrue = () => { click = true; }
  // var clickFalse = () => { click = false; }
  // var unselectTeachers = () => {
  //   var teacherSelected = document.getElementsByClassName('selected-teacher');
  //   console.log(teacherSelected.length);
  //   if (click) {
  //     console.log("click");
  //     if (teacherSelected.length > 0) {
  //       console.log("hola");
  //       removeClass('.ancestor', 'ancestor');
  //       removeClass('.descendant', 'descendant');
  //       removeClass('.selected', 'selected');
  //       removeClass('.selected-teacher', 'selected-teacher');
  //     }
  //   }
  // };

  // timeline.on('mouseDown', clickTrue);
  // timeline.on('mouseMove', clickFalse);
  // timeline.on('mouseUp', unselectTeachers);
}
