import ancestorsIndex from '../data/ancestors_index.json';
import LeaderLine from 'leader-line-new';
export var removeClass;

// Convert string of ids to array.
var getDescendantIds = (idString) => {
  let ids = idString.split(',');
  // In case there are no descendants, return an empty array.
  // Splitting an empty string returns an array with one empty string.
  if (ids[0].length == 0) { ids = []; }
  return ids;
};

var getAncestorIds = (teacherId) => {
  if (typeof(ancestorsIndex[teacherId]) == 'undefined') {
    return [];
  }
  else {
    return ancestorsIndex[teacherId];
  }
};

var removeClass = (selector, myClass) => {
  document.querySelectorAll(selector).forEach(e => e.classList.remove(myClass));
};

// Params: Array, String
var addClass = (ids, cssClass) => {
  for (let id of ids) {
    document.getElementById(id).classList.add(cssClass);
  }
};

function deleteSvgs() {
  // Note: leader-lines are outside of #visualization (and #main!!!) div.
  // Apparently does not work in Explorer.
  document.querySelectorAll('.leader-line').forEach(e => e.remove());
}

// Params: String
 function addAncestors(teacherId) {
  // SVG Leaderlines must be deleted first.
  deleteSvgs();
  if (teacherId > 0) {
    let ancestorIds = getAncestorIds(teacherId);
    addClass(ancestorIds, 'ancestor');
    var llOptions = {
      // shinshu, only slightly different from enji.
      'color': 'rgb(171, 59, 58)',
      'size': 2,
      'path': 'magnet',
      'endSocket': 'middle',
    };
    for (var i = 0, len = ancestorIds.length; i < len - 1; i++) {
      new LeaderLine(
        document.getElementById(ancestorIds[i]),
        document.getElementById(ancestorIds[i+1]),
        llOptions
      );
    }
    // Leaderline between last ancestor and selected teacher
    new LeaderLine(
      document.getElementById(ancestorIds[ancestorIds.length - 1]),
      document.getElementById(teacherId),
      llOptions
    );
  }
}

// Params: String (from data-descendant_ids)
var addDescendants = (descendantIds) => {
  let ids = getDescendantIds(descendantIds);
  addClass(ids, 'descendant');
}

var teacherMarker = (teacherDiv) => {
  const teacherMarker = document.createElement('div');
  const parentElement = teacherDiv.closest('.teacher-group');
  var style = window.getComputedStyle(teacherDiv);
  var matrix = new DOMMatrix(style.transform);
  // Don't use class since it makes removal more difficult and inefficient
  teacherMarker.id = "teacher-marker";
  teacherMarker.classList.add("teacher-marker");
  var teacherClasses = Array.from(teacherDiv.classList);
  // Add *-fade class to teacher-marker.
  var patt = new RegExp(/\w+-fade/);
  const match = teacherClasses.find(value => patt.test(value));
  if (match != undefined) {
    // Use class here since it makes applying styles easier!
    teacherMarker.classList.add(match);
  }
  teacherMarker.style.width = `${teacherDiv.offsetWidth}px`;
  teacherMarker.style.height = `${parentElement.offsetHeight}px`;
  teacherMarker.style.transform = matrix;
  parentElement.appendChild(teacherMarker);
}
var removeTeacherMarker = () => {
  let teacherMarker = document.getElementById('teacher-marker');
  if (teacherMarker != undefined) {
    teacherMarker.remove();
  }
}
var toggleLineage = (teacherId) => {
  const teacherDiv = event.target.closest('.teacher');

  // Clicked object and object that remembers state cannot be the same.
  // Therefore, use vis-item-visible-frame which is almost unclickable.
  const cookie = teacherDiv.querySelector('.vis-item-visible-frame');
  removeClass('.ancestor', 'ancestor');
  removeClass('.descendant', 'descendant');
  removeTeacherMarker();
  // If same teacher is clicked
  if (cookie.classList.contains('selected')) {
    cookie.classList.remove('selected');
    teacherDiv.classList.remove('selected-teacher');
    deleteSvgs();
  }
  else {
    removeClass('.selected-teacher', 'selected-teacher');
    // Using .vis-selected does not work because vis-selected fires before all this runs.
    teacherDiv.classList.add('selected-teacher');
    addAncestors(teacherId);
    addDescendants(teacherDiv.getAttribute('data-descendant_ids'));
    removeClass('.vis-item-visible-frame', 'selected');
    cookie.classList.add('selected');
    teacherMarker(teacherDiv);
  }
};

export class TeacherDivs {
  constructor(el){
    this.el = el;
  }

  prepDivs() {
    for (var i = 0, len = this.el.length; i < len; i++) {
      let item_id = this.el[i].getAttribute('data-item_id')
      // HTML id is set here from data-item_id.
      this.el[i].setAttribute('id', item_id);
      this.el[i].addEventListener("click",
        // Must use item_id, since item_id (and not array id etc) identifies teacher.
        toggleLineage.bind(this, item_id)
      );
    }
  };
}
