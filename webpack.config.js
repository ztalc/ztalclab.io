const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const svgToMiniDataURI = require('mini-svg-data-uri');

module.exports = [
  // Development mode produces timeline via js.
  // Production mode produces timeline via static html.
  // That is the only difference.
  {
    mode: 'development',
    entry: {
      dev: ['./source/javascripts/dev.js'],
      style: ['./source/stylesheets/site.css.scss'],
    },
    output: {
      path: path.resolve(__dirname, '.tmp/dist'),
      filename: '[name].min.js',
      sourceMapFilename: "[name].js.map",
    },
    devtool: "source-map",
    module: {
      rules: [
        {
          test: /\.(sa|sc|c)ss$/,
          use: [
            MiniCssExtractPlugin.loader,
            'css-loader',
            'sass-loader',
          ],
        },
        {
          test: /\.svg$/i,
          use: [
            {
              loader: 'url-loader',
              options: {
                generator: (content) => svgToMiniDataURI(content.toString()),
              },
            },
          ],
        },
        {
          test: /\.(png|jpg|gif)$/i,
          use: [
            {
              loader: 'url-loader',
              options: {
                limit: 8192,
              },
            },
          ],
        }
      ]
    },
    plugins: [new MiniCssExtractPlugin()],
  },
  {
    mode: 'production',
    entry: {
      site: ['./source/javascripts/site.js'],
      style: ['./source/stylesheets/site.css.scss'],
    },
    output: {
      path: path.resolve(__dirname, '.tmp/dist'),
      filename: '[name].min.js',
      sourceMapFilename: "[name].js.map",
    },
    devtool: "source-map",
    module: {
      rules: [
        {
          test: /\.(sa|sc|c)ss$/,
          use: [
            MiniCssExtractPlugin.loader,
            'css-loader',
            'sass-loader',
          ],
        },
        {
          test: /\.svg$/i,
          use: [
            {
              loader: 'url-loader',
              options: {
                generator: (content) => svgToMiniDataURI(content.toString()),
              },
            },
          ],
        },
        {
          test: /\.(png|jpg|gif)$/i,
          use: [
            {
              loader: 'url-loader',
              options: {
                limit: 8192,
              },
            },
          ],
        }
      ]
    },
    plugins: [new MiniCssExtractPlugin()],
  }];
