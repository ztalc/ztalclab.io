var toggleLineage = (teacherId) => {
  const teacherDiv = event.target.closest('.teacher');
  // Clicked object and object that remembers state cannot be the same.
  // Therefore, use vis-item-visible-frame which is almost unclickable.
  const cookie = teacherDiv.querySelector('.vis-item-visible-frame');
  removeClass('.ancestor', 'ancestor');
  removeClass('.descendant', 'descendant');
  deleteSvgs();
  // If same teacher is clicked
  if (cookie.classList.contains('selected')) {
    cookie.classList.remove('selected');
    teacherDiv.classList.remove('selected-teacher');
  }
  else {
    removeClass('.selected-teacher', 'selected-teacher');
    // Using .vis-selected does not work because vis-selected fires before all this runs.
    teacherDiv.classList.add('selected-teacher');
    addAncestors(teacherId);
    addDescendants(teacherDiv.getAttribute('data-descendant_ids'));
    removeClass('.vis-item-visible-frame', 'selected');
    cookie.classList.add('selected');
  }
  // event.stopPropagation();
};

export class TeacherDivs {
  constructor(el){
    this.el = el;
  }

  prepDivs() {
    for (var i = 0, len = this.el.length; i < len; i++) {
      let item_id = this.el[i].getAttribute('data-item_id')
      // HTML id is set here from data-item_id.
      this.el[i].setAttribute('id', item_id);
      this.el[i].addEventListener("click",
        // Must use item_id, since item_id (and not array id etc) identifies teacher.
        toggleLineage.bind(this, item_id)
      );
    }
  };
}
