// Usage: const teacherItems = new Items(teachersJSON, "teacher");
// Takes items in json array and outputs vis-timeline compatible array.

// Attributes cannot be accessed after it has been processed by vis-timeline.
// Array order is not preserved by vis-timeline.
// Group determines order.
function findTemplate(name){
  const templates = [
    { item: "dynasties",
      type: "background",
      group: 0,
      className: "dynasty",
    },
    { item: "teachers",
      // Leave id as string.
      order_id: "0",
      group: 1,
      className: "teacher",
    },
    // { item: "events",
    //   // TODO: Events may not need order_id.
    //   // order_id: "1000",
    //   group: 1,
    //   className: "event",
    // },
    // { item: "rulers",
    //   type: "background",
    //   group: 2,
    //   className: "ruler",
    // },
  ];

  for (let temp of templates) {
    if (temp.item == name) {
      return temp;
    }
  }
};

// Add other attributes to items array.
function applyTemplate(items, name){
  let template = findTemplate(name);
  for (var i = 0, len = items.length; i < len; i++) {
    // This does not create html ids
    // This may be used in sort algorithm. If unused, get rid of it.
    items[i].order_id = template.order_id + i;
    items[i].group = template.group;
    items[i].type = template.type;
    // Does not appear to change anything.
    //items[i].limitSize = true;

    // Add extra classes to timeline items
    if (typeof items[i].className == 'undefined') {
      items[i].className = template.className;
    }
    else {
      items[i].className += ' ' + template.className;
    }
  }
  return items;
}

export class Items {
  constructor(items, name){
    this.name = name,
    this.items = applyTemplate(items, name);
  }
}
